<?php

use App\Content;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/quiz', function () {
    return view('quiz');
});
Route::get('/', function () {
    return view('index');
});
Route::get('/pdf/page', function () {
    return view('pdf');
});

Route::post('/form/submit', function (Request $request) {
    $result = 0;
    $errors = [];
    if ($request->name1 == "Des salariés qui travaillent dans un même but"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Qu’est-ce qu’une entreprise ?' Réponse: 'Des salariés qui travaillent dans un même but'";
    }
    if ($request->name2 == "De la Gestion du Personnel"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Dans une entreprise, le service des Ressources Humaines se charge ?' Réponse: 'De la Gestion du Personnel'";
    }
    if ($request->name3 == "Organiser le transport des marchandises"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Dans une entreprise, la fonction logistique consiste à ?' Réponse: 'Organiser le transport des marchandises'";
    }
    if ($request->name4 == "Le service recherche et développement"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Comment s’appelle le service qui conçoit de nouveaux produits ?' Réponse: 'Le service recherche et développement '";
    }
    if ($request->name5 == "La fonction de direction"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Quelle fonction occupe le chef d’entreprise?' Réponse: 'La fonction de direction'";
    }
    if ($request->name6 == "L’organigramme"){
        $result = $result + 1;
    }else{
        $errors[] = "Q: 'Le schéma qui décrit l’organisation des services de l’entreprise s’appelle?' Réponse: 'L’organigramme'";
    }
    if ($result == 6){
        return view('success');
    }else{
        return view('error', compact('errors'));
    }
})->name('form.submit');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/video/links', 'HomeController@videolinks')->name('video.links');
Route::post('/video/store', 'HomeController@videostore')->name('content.store');
Route::get('/download', function (){
    $content = Content::find(1);
    //PDF file is stored under project/public/download/info.pdf
    $file= public_path(). "/".$content->pdf;

    $headers = array(
        'Content-Type: application/pdf',
    );

    return Response::download($file, '1.pdf', $headers);
})->name('download');
Route::get('/admin/pdf', 'HomeController@adminPdf')->name('admin.pdf');
Route::post('/pdf/store', 'HomeController@pdfstore')->name('pdf.store');

Route::get('/update-Password', [
    'uses' => 'HomeController@password_change',
    'as' => 'change.password'
]);
Route::post('/update-password/store', [
    'uses' => 'HomeController@password_update',
    'as' => 'update.password'
]);
