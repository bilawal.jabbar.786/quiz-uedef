@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Teneur</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Teneur</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form action="{{route('content.store')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 1 </label>
                                        <input type="text" class="form-control" name="t1" value="{{$content->t1}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v1" value="{{$content->v1}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 2 </label>
                                        <input type="text" class="form-control" name="t2" value="{{$content->t2}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v2" value="{{$content->v2}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 3 </label>
                                        <input type="text" class="form-control" name="t3" value="{{$content->t3}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v3" value="{{$content->v3}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 4 </label>
                                        <input type="text" class="form-control" name="t4" value="{{$content->t4}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v4" value="{{$content->v4}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 5 </label>
                                        <input type="text" class="form-control" name="t5" value="{{$content->t5}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v5" value="{{$content->v5}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 6 </label>
                                        <input type="text" class="form-control" name="t6" value="{{$content->t6}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v6" value="{{$content->v6}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 7 </label>
                                        <input type="text" class="form-control" name="t7" value="{{$content->t7}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v7" value="{{$content->v7}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Titre 8 </label>
                                        <input type="text" class="form-control" name="t8" value="{{$content->t8}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Identifiant vidéo </label>
                                        <input type="text" class="form-control" name="v8" value="{{$content->v8}}" required>
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->
        </section>
    </div>
@endsection
