<div class="row">
    <div class="col-md-3">
        <img class="w100" src="{{asset('logo.jpg')}}" alt="">
    </div>
    <div class="col-md-2">
        <a href="https://www.ude-medef.com/fr/actualite/une-entreprise-cest-quoi"><button class="mybtn mt20">ACCUEIL</button></a>
    </div>
    <div class="col-md-2">
        <a href="{{url('/')}}"><button class="mybtn mt20">VIDEO</button></a>
    </div>
    <div class="col-md-2">
        <a href="{{url('/quiz')}}"><button class="mybtn mt20">QUIZ</button></a>
    </div>
    <div class="col-md-2">
        <a href="{{url('/pdf/page')}}"><button class="mybtn mt20">LIVRET</button></a>
    </div>
</div>
