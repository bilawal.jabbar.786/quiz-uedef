<style>
    .w100{
        width: 100%;
    }
    .mybtn {
        background-color: #602156; /* Green */
        border: none;
        color: white;
        padding: 15px 70px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }
    .mybtn1 {
        background-color: #602156; /* Green */
        border: none;
        color: white;
        padding: 15px 85px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }
    .mt20{
        margin-top: 20px;
    }
    .hw200{
        height: 200px;
        width: 200px;
        margin-left: 100px;
    }
    .cwhite{
        color: white;
    }
    .h100w200{
        height: 100px;
        width: 200px;
    }
    /* Mark input boxes that gets an error on validation: */
    input.invalid {
        background-color: #ffdddd;
    }

    /* Hide all steps by default: */
    .tab {
        display: none;
    }


    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #602156;
        border: none;
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        background-color: #602156;
    }
    .mt300{
        margin-top: 300px;
    }
    .mt400{
        margin-top: 400px;
    }
    .quizbox{
        background-color: #faf2f2;
        border-radius: 10px;
        padding: 50px;
    }
    .demo-iframe-holder {
        width: 100%;
        height: 680px;
        -webkit-overflow-scrolling: touch;
        overflow-y: scroll;
    }

    .demo-iframe-holder iframe {
        height: 100%;
        width: 100%;
    }
    .mt40{
        margin-top: 40px;
    }
    .seta{
        color: inherit;
        text-decoration: none;
    }
    .seta:hover{
        color: black;
        text-decoration: none;
    }
    .dnone{
        display: none;
    }
    .ytp-impression-link {
        display: none !important;
    }
    @media only screen and (max-width: 600px) {
        .bgimg {
            background-image: none;
        }
        .mybtn{
            width: 100%;
        }
        .hw200{
            margin-left: 70px;
        }
        .video{
            width: 100% !important;
            height: 250px;
        }
        .cwhite{
            color: black; !important;
        }
        .mt300{
            margin-top: 10px !important;
        }
        #prevBtn{
            margin-bottom: 20px;
        }
    }
</style>
