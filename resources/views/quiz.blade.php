<!DOCTYPE html>
<html lang="en">
<head>
    <title>MEDEF</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" href="{{asset('fav.png')}}">
    <link rel="apple-touch-icon" href="{{asset('fav.png')}}">
    <link rel="icon" href="{{asset('fav.png')}}">

    <style>
        body {
            background-image:url("{{asset('05.jpg')}}");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>
    @include('include.css')
</head>
<body class="bgimg">

<div class="container-fluid">
    @include('include.header')
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
{{--            <a href="https://www.ude-medef.com/fr/actualite/une-entreprise-cest-quoi"> <img class="w100 mt20 h100w200" src="{{asset('06.png')}}" alt=""></a>--}}
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div class="row mt300">
        <div class="col-md-8 quizbox">
            <form id="regForm" action="{{route('form.submit')}}" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            <li>You Must FIll All Questions</li>
                        </ul>
                    </div>
            @endif
            <!-- One "tab" for each step in the form: -->
                <div class="tab">
                    <h3 style="font-weight: 900">Qu’est-ce qu’une entreprise ?</h3>
                    <br>
                    <p><input type="radio" name="name1" value="Des salariés qui exercent le même métier" required> Des salariés qui exercent le même métier</p>
                    <p><input type="radio" name="name1" value="Des salariés qui travaillent chacun pour soi"> Des salariés qui travaillent chacun pour soi </p>
                    <p><input type="radio" name="name1" value="Des salariés qui travaillent dans un même but"> Des salariés qui travaillent dans un même but </p>
                    <br>
                    <b class="name1true dnone">Votre réponse est "Vrai"</b>
                    <b class="name1false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name1comment dnone">
                    <b>Commentaires :</b><br>
                    Une entreprise rassemble des personnes qui exercent des métiers différents mais unies dans un même but : produire et vendre des biens matériels ou des services à des clients. L’entreprise est une somme de compétences, mais surtout une somme de comportements et de savoir-être.
                    </div>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Dans une entreprise, le service des Ressources Humaines se charge ?</h3>
                    <br>
                    <p><input type="radio" name="name2" value="De la Relation Client" required> De la Relation Client</p>
                    <p><input type="radio" name="name2" value="De la Gestion du Personnel"> De la Gestion du Personnel</p>
                    <p><input type="radio" name="name2" value="De la Gestion des Achats"> De la Gestion des Achats </p>
                    <br>
                    <b class="name2true dnone">Votre réponse est "Vrai"</b>
                    <b class="name2false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name2comment dnone">
                        <b>Commentaires :</b><br>
                        Le service des Ressources Humaines s’occupe du personnel de l’entreprise: recrutement, contrats de travail, congés, évolution des carrières, formation interne… Plus l’entreprise est grande, plus ce service est étoffé!
                    </div>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Dans une entreprise, la fonction logistique consiste à ?</h3>
                    <br>
                    <p><input type="radio" name="name3" value="Réaliser des études de marché" required> Réaliser des études de marché</p>
                    <p><input type="radio" name="name3" value="Organiser le transport des marchandises"> Organiser le transport des marchandises</p>
                    <p><input type="radio" name="name3" value="Contrôler la qualité des produits"> Contrôler la qualité des produits</p>
                    <br>
                    <b class="name3true dnone">Votre réponse est "Vrai"</b>
                    <b class="name3false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name3comment dnone">
                        <b>Commentaires :</b><br>
                        La logistique organise le stockage et le transport des marchandises. Les études de marché sont réalisées par le service marketing. Le service production fabrique les produits et en contrôle la qualité.
                    </div>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Comment s’appelle le service qui conçoit de nouveaux produits ?</h3>
                    <br>
                    <p><input type="radio" name="name4" value="Le service marketing et commercial" required> Le service marketing et commercial</p>
                    <p><input type="radio" name="name4" value="Le service des achats"> Le service des achats </p>
                    <p><input type="radio" name="name4" value="Le service recherche et développement"> Le service recherche et développement </p>
                    <br>
                    <b class="name4true dnone">Votre réponse est "Vrai"</b>
                    <b class="name4false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name4comment dnone">
                        <b>Commentaires :</b><br>
                        Le service Recherche et Développement réalise des études et des tests pour concevoir un produit avant qu’il ne soit commercialisé. Très souvent, il crée un prototype, c’est-à-dire un premier exemplaire du produit qui est testé avant d’être fabriqué en série.
                    </div>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Quelle fonction occupe le chef d’entreprise?</h3>
                    <br>
                    <p><input type="radio" name="name5" value="La fonction de direction" required> La fonction de direction </p>
                    <p><input type="radio" name="name5" value="La fonction de surveillance"> La fonction de surveillance </p>
                    <p><input type="radio" name="name5" value="La fonction financière"> La fonction financière</p>
                    <br>
                    <b class="name5true dnone">Votre réponse est "Vrai"</b>
                    <b class="name5false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name5comment dnone">
                        <b>Commentaires :</b><br>
                        La fonction de direction est la fonction prédominante d’une entreprise. Elle s’applique à définir les choix stratégiques de l’entreprise. Elle organise l’entreprise ; elle met en place des sous-système, structures, méthodes et procédures nécessaires pour atteindre les objectifs. Elle rassemble, gère anime les forces et les ressources nécessaires à la réalisation du projet d’entreprise.
                    </div>
                </div>
                <br>
                <br>
                <div class="tab">
                    <h3 style="font-weight: 900">Le schéma qui décrit l’organisation des services de l’entreprise s’appelle? (Question bonus)</h3>
                    <br>
                    <p><input type="radio" name="name6" value="Le trombinoscope" required> Le trombinoscope </p>
                    <p><input type="radio" name="name6" value="La carte d’entreprise"> La carte d’entreprise </p>
                    <p><input type="radio" name="name6" value="L’organigramme"> L’organigramme</p>
                    <br>
                    <b class="name6true dnone">Votre réponse est "Vrai"</b>
                    <b class="name6false dnone">Votre réponse est "Faux"</b>
                    <br>
                    <br>
                    <div class="name6comment dnone">
                        <b>Commentaires :</b><br>
                        L’organisation d’une entreprise est décrite dans un organigramme. Celui-ci présente sous forme de schéma tous les services d’une entreprise mais aussi les liens entre eux. Il précise la répartition des tâches et les relations hiérarchiques (direction, chef de service…).
                    </div>
                </div>
                <br>
                <div style="text-align: right">
                    <div>
                        <button style="background-color: green" type="button" class="btn btn-primary mybtn1 valider">Valider</button>
                     </div>
                </div>
                <br>
                <div style="text-align: right">
                    <div>
                        <button type="button"  class="btn btn-primary mybtn1" id="prevBtn" onclick="nextPrev(-1)">Précédent</button>
                        <button type="button" style="background-color: #462665" class="btn btn-success mybtn1" id="nextBtn" onclick="nextPrev(1)">Suivant</button>
                    </div>
                </div>
                <!-- Circles which indicates the steps of the form: -->
                <div style="text-align:right;margin-top:40px;">
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                </div>
            </form>
        </div>
        <div class="col-md-4">

        </div>
    </div>


    <br>
    <br>
    <br>
    <br>
    <br>

</div>
<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Soumettre";
        } else {
            document.getElementById("nextBtn").innerHTML = "Suivant";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }

    $(".valider").click(function(){
        if (currentTab == 0){
            var name1 = $('input[name="name1"]:checked').val();
            if (name1 == "Des salariés qui travaillent dans un même but"){
               $('.name1comment').show();
               $('.name1true').show();
                $('.name1false').hide();
            }else {
                $('.name1comment').show();
                $('.name1false').show();
                $('.name1true').hide();
            }
        }
        if (currentTab == 1){
            var name2 = $('input[name="name2"]:checked').val();
            if (name2 == "De la Gestion du Personnel"){
               $('.name2comment').show();
               $('.name2true').show();
                $('.name2false').hide();
            }else {
                $('.name2comment').show();
                $('.name2false').show();
                $('.name2true').hide();
            }
        }
        if (currentTab == 2){
            var name3 = $('input[name="name3"]:checked').val();
            if (name3 == "Organiser le transport des marchandises"){
               $('.name3comment').show();
               $('.name3true').show();
                $('.name3false').hide();
            }else {
                $('.name3comment').show();
                $('.name3false').show();
                $('.name3true').hide();
            }
        }
        if (currentTab == 3){
            var name4 = $('input[name="name4"]:checked').val();
            if (name4 == "Le service recherche et développement"){
               $('.name4comment').show();
               $('.name4true').show();
                $('.name4false').hide();
            }else {
                $('.name4comment').show();
                $('.name4false').show();
                $('.name4true').hide();
            }
        }
        if (currentTab == 4){
            var name5 = $('input[name="name5"]:checked').val();
            if (name5 == "La fonction de direction"){
               $('.name5comment').show();
               $('.name5true').show();
                $('.name5false').hide();
            }else {
                $('.name5comment').show();
                $('.name5false').show();
                $('.name5true').hide();
            }
        }
        if (currentTab == 5){
            var name6 = $('input[name="name6"]:checked').val();
            if (name6 == "L’organigramme"){
               $('.name6comment').show();
               $('.name6true').show();
                $('.name6false').hide();
            }else {
                $('.name6comment').show();
                $('.name6false').show();
                $('.name6true').hide();
            }
        }
    });
</script>
</body>
</html>
