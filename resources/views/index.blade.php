<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="{{asset('fav.png')}}">
    <?php
    $content = \App\Content::find(1);
    ?>
    <title>MEDEF</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="{{asset('fav.png')}}">
        <link rel="apple-touch-icon" href="{{asset('fav.png')}}">
    <style>
        body {
            background-image:url("{{asset('08.jpg')}}");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>
    @include('include.css')

</head>
<body class="bgimg">

<div class="container-fluid">
   @include('include.header')
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
{{--            <a href="https://www.ude-medef.com/fr/actualite/une-entreprise-cest-quoi"><img class="w100 mt20 h100w200" src="{{asset('06.png')}}" alt=""></a>--}}
        </div>
        <div class="col-md-4">
            <img class="w100 mt20" src="{{asset('1.png')}}" alt="">
        </div>
        <div class="col-md-4">
{{--            <img class="w100 hw200" src="{{asset('3.jpg')}}" alt="">--}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8 mt40">
            <iframe class="video" id="mainvideo" width="100%" height="400"
                    src="https://www.youtube.com/embed/{{$content->v1}}?rel=0">
            </iframe>
        </div>
        <div class="col-md-2">

        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mt20">
            <div class="cwhite">
                <a class="video1 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t1}}</a>
            </div>
            <div class="cwhite">
                <a class="video2 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t2}}</a>
            </div>
            <div class="cwhite">
                <a class="video3 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t3}}</a>
            </div>
            <div class="cwhite">
                <a class="video4 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t4}}</a>
            </div>
            <div class="cwhite">
                <a class="video5 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t5}}</a>
            </div>
            <div class="cwhite">
                <a class="video6 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t6}}</a>
            </div>
            <div class="cwhite">
                <a class="video7 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t7}}</a>
            </div>
            <div class="cwhite">
                <a class="video8 seta"><img style="height: 50px" src="{{asset('4.png')}}" alt="">- {{$content->t8}}</a>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
    <br>
    <br>
    <br>
    <br>

</div>
<script>
    $(".video1").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v1}}?rel=0';
    })
    $(".video2").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v2}}?rel=0';
    })
    $(".video3").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v3}}?rel=0';
    })
    $(".video4").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v4}}?rel=0';
    })
    $(".video5").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v5}}?rel=0';
    })
    $(".video6").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v6}}?rel=0';
    })
    $(".video7").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v7}}?rel=0';
    })
    $(".video8").click(function(){
        document.getElementById('mainvideo').src='https://www.youtube.com/embed/{{$content->v8}}?rel=0';
    })
    document.getElementsByTagName('iframe')[0].contentWindow.getElementsByClassName('ytp-watch-later-button')[0].style.display = 'none';
    document.getElementsByTagName('iframe')[0].contentWindow.getElementsByClassName('ytp-impression-link')[0].style.display = 'none';
</script>
<script type="text/javascript" src="https://chatterpal.me/build/js/chatpal.js?8.1" crossorigin="anonymous" data-cfasync="false"></script>
<script>
    var chatPal = new ChatPal({embedId: 'Atk2MfZcc7Fz', remoteBaseUrl: 'https://chatterpal.me/', version: '8.1'});
</script>
</body>
</html>
