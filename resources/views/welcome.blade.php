<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Quiz</title>
    <style>
        body {
            background-image:url("img.jpg");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .section{
            padding-bottom: 100px; padding-top: 350px
        }
        .p-l100{
            padding-left: 100px;
        }
        .mybix{
            background-color: white; padding: 20px; border-radius: 20px
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }


        #prevBtn {
            background-color: #bbbbbb;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }
        @media only screen and (max-width: 600px) {
            .bgimg {
                background-image: none;
                background-color: #fafafa;
            }
            .p-l100{
                padding-left: 0px !important;
            }
            .section{
                padding-bottom: 200px; padding-top: 0px
            }
            .mds{
                display: block !important;
            }
            .mybix {
                padding: 40px;
            }
        }
    </style>
</head>
<body class="bgimg">
<div class="section">
    <img class="mds" src="{{asset('logo.jpg')}}" style="width: 100%; display: none" alt="">
    <img class="mds" src="{{asset('1.png')}}" style="width: 100%; display: none" alt="">
    <div class="row p-l100">
        <div class="col-md-6 mybix">
            <form id="regForm" action="{{route('form.submit')}}" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                          <li>You Must FIll All Questions</li>
                        </ul>
                    </div>
             @endif
                <!-- One "tab" for each step in the form: -->
                <div class="tab">
                    <h3 style="font-weight: 900">Quelle est l’activité de Guadeloupe Plastic?</h3>
                    <br>
                    <p><input type="radio" name="name1" value="Impression de boîtiers électriques" required> Impression de boîtiers électriques</p>
                    <p><input type="radio" name="name1" value="Impression d’affiches"> Impression d’affiches</p>
                    <p><input type="radio" name="name1" value="Impression d’autocollants"> Impression d’autocollants </p>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Quelles sont les missions du service marketing ?</h3>
                    <br>
                    <p><input type="radio" name="name2" value="Vente" required> Vente</p>
                    <p><input type="radio" name="name2" value="Etude de marché"> Etude de marché</p>
                    <p><input type="radio" name="name2" value="Distribution"> Distribution </p>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Qui définit le projet ?</h3>
                    <br>
                    <p><input type="radio" name="name3" value="Assistante" required> Assistante</p>
                    <p><input type="radio" name="name3" value="Commercial"> Commercial</p>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Quel service identifie les besoins en recrutement ?</h3>
                    <br>
                    <p><input type="radio" name="name4" value="Ressources humaines" required> Ressources humaines</p>
                    <p><input type="radio" name="name4" value="Direction"> Direction</p>
                    <p><input type="radio" name="name4" value="Juridique"> Juridique</p>
                </div>
                <div class="tab">
                    <h3 style="font-weight: 900">Quel est le nom du premier client de Guadeloupe Plastic?</h3>
                    <br>
                    <p><input type="radio" name="name5" value="Guadeloupe Plastic" required> Guadeloupe Plastic </p>
                    <p><input type="radio" name="name5" value="Smart up"> Smart up </p>
                    <p><input type="radio" name="name5" value="Magasins Clémentine"> Magasins Clémentine</p>
                </div>
                <div style="overflow:auto;">
                    <div style="text-align: center;">
                        <button type="button"  class="btn btn-primary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" style="background-color: #462665" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    </div>
                </div>
                <!-- Circles which indicates the steps of the form: -->
                <div style="text-align:center;margin-top:40px;">
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                </div>
            </form>
        </div>
        <div class="col-md-6">

        </div>
    </div>

</div>
<!--    <div style="background-image:url('img.jpg'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; ">
        <div style="padding-top: 800px">

        </div>
    </div>-->
<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
</body>
</html>
