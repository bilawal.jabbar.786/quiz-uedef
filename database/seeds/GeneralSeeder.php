<?php

use App\Content;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Admin',
            'role'     => '1',
            'email'    => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
        Content::create([
            'v1' => 'aq_ZjKVOju0',
            'v2' => 'r76XF8OpaW4',
            'v3' => 'BX3OrHwQixg',
            'v4' => 'DcSG4jKcFXM',
            'v5' => 'I2qSJle8Iiw',
            'v6' => 'AAWDexTp_A8',
            'v7' => 'cGRX0uTfMUQ',
            'v8' => 'v49LGRI2Jbc',
            't1' => 'Bienvenue dans lentreprise',
            't2' => 'Quest-ce quune entreprise?',
            't3' => 'Entreprises selon le nombre de salariés',
            't4' => 'Une entreprise, des métiers',
            't5' => 'Comment fonctionne une entreprise ?',
            't6' => 'Témoignages',
            't7' => 'Un projet, des engagements',
            't8' => 'Lentreprise un acteur responsable de notre notre société',
            'pdf' => 'pdf/pdf.pdf'
        ]);
    }
}
