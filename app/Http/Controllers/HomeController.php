<?php

namespace App\Http\Controllers;

use App\Content;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function videolinks(){
        $content = Content::find(1);
        return view('video.links', compact('content'));
    }
    public function videostore(Request $request){
        $content = Content::find(1);
        $content->t1 = $request->t1;
        $content->t2 = $request->t2;
        $content->t3 = $request->t3;
        $content->t4 = $request->t4;
        $content->t5 = $request->t5;
        $content->t6 = $request->t6;
        $content->t7 = $request->t7;
        $content->t8 = $request->t8;

        $content->v1 = $request->v1;
        $content->v2 = $request->v2;
        $content->v3 = $request->v3;
        $content->v4 = $request->v4;
        $content->v5 = $request->v5;
        $content->v6 = $request->v6;
        $content->v7 = $request->v7;
        $content->v8 = $request->v8;
        $content->update();
        $notification = array(
            'messege' => 'Mise à jour du statut!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function adminPdf(){
        return view('admin.pdf');
    }
    public function pdfstore(Request $request){
        $content = Content::find(1);

        if ($request->hasfile('pdf')) {
            $image3 = $request->file('pdf');
            $name = time() . 'pdf' . '.' . $image3->getClientOriginalExtension();
            $destinationPath = 'pdf/';
            $image3->move($destinationPath, $name);
            $content->pdf = 'pdf/' . $name;
        }
        $content->update();
        $notification = array(
            'messege' => 'Mise à jour du statut!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);

    }
    public function password_change() {
        return view ('admin.updatepassword');
    }

    public function password_update(Request $request)
    {
        $password=Auth::user()->password;
        $oldpass=$request->oldpass;
        $newpass=$request->password;
        $confirm=$request->password_confirmation;
        if (Hash::check($oldpass,$password)) {
            if ($newpass === $confirm) {
                $user=User::find(Auth::id());
                $user->password=Hash::make($request->password);
                $user->save();
                Auth::logout();
                $notification=array(
                    'messege'=>'Le mot de passe a été changé avec succès ! Connectez-vous maintenant avec votre nouveau mot de passe',
                    'alert-type'=>'success'
                );
                return Redirect()->route('login')->with($notification);
            }else{
                $notification=array(
                    'messege'=>'Le nouveau mot de passe et la confirmation du mot de passe ne correspondent pas!',
                    'alert-type'=>'error'
                );
                return Redirect()->back()->with($notification);
            }
        }else{
            $notification=array(
                'messege'=>'Lancien mot de passe ne correspond pas!',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }

    }
}
